package com.thrashed.sfutimetable.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thrashed.sfutimetable.Adapters.RVAdapter;
import com.thrashed.sfutimetable.Data.Lesson;
import com.thrashed.sfutimetable.Data.realmData;
import com.thrashed.sfutimetable.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;

public class SbFragment extends Fragment {

    private List<Lesson> Lessons;
    private RecyclerView rv;
    private Realm mrealm;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mrealm = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //setHasOptionsMenu(true);
        //return inflater.inflate(R.layout.recyclerview_activity, container, false);
        View rootView =
                inflater.inflate(R.layout.recyclerview_activity, container, false);
        rv = rootView.findViewById(R.id.rv);
        // Context Context = getContext();
        LinearLayoutManager llm = new LinearLayoutManager(rootView.getContext());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        initializeData();
        initializeAdapter();

        return rootView;

    }

    private void initializeData() {
        Lessons = new ArrayList<>();
        final RealmResults<realmData> paras;
        if (Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) % 2 == 0) {
            paras = mrealm.where(realmData.class)
                    .equalTo("day", 6)
                    .equalTo("week", false)
                    .findAll();
        } else {
            paras = mrealm.where(realmData.class)
                    .equalTo("day", 6)
                    .equalTo("week", true)
                    .findAll();
        }
        for (realmData para : paras) {
            Lessons.add(new Lesson(para.getName(), para.getCabinet(), para.getTime()));
        }
    }

    private void initializeAdapter() {
        RVAdapter adapter = new RVAdapter(Lessons);
        rv.setAdapter(adapter);
    }
}




