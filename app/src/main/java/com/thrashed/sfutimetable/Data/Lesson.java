package com.thrashed.sfutimetable.Data;

public class Lesson {

    public String name;
    public String cabinet;
    public String time;


    public Lesson(String name, String cabinet, String time) {
        this.name = name;
        this.cabinet = cabinet;
        this.time = time;
    }


    @Override
    public String toString() {
        return time + " " + cabinet + " " + name;
    }
}

