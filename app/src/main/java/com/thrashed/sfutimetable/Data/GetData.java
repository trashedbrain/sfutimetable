package com.thrashed.sfutimetable.Data;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;


public class GetData {


    public void getWebsite() {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                Realm realm = Realm.getDefaultInstance();


                try {
                    Document doc = Jsoup.connect("http://edu.sfu-kras.ru/timetable?group=РФ16-33Б").get();


                    Element Table = doc.select("table").get(1); //Выбираем таблицу с расписанием
                    Elements Rows = Table.select("tr");//Выбираем строки в таблице
                    int daycounter = 0;
                    final List<Lesson> Lessons = new ArrayList<>();


                    for (int i = 0; i < Rows.size(); i++) {//Пробег по всем строкам

                        Element row = Rows.get(i); //по номеру индекса получает строку
                        Elements cols = row.select("td");// разбиваем полученную строку по тегу  на столбы

                        switch (row.select("tr.heading").text()) {
                            case "Понедельник":
                                daycounter = 1;
                                break;
                            case "Вторник":
                                daycounter = 2;
                                break;
                            case "Среда":
                                daycounter = 3;
                                break;
                            case "Четверг":
                                daycounter = 4;
                                break;
                            case "Пятница":
                                daycounter = 5;
                                break;
                            case "Суббота":
                                daycounter = 6;
                                break;

                        }

                        if (cols.size() == 0) {
                            continue; //Избегаем NullPointer при пустых строках
                        }


                        if (cols.size() < 4 && cols.get(2).hasText()) {

                            realm.beginTransaction();
                            realmData realmDat = realm.createObject(realmData.class);
                            realmDat.setDay(daycounter);
                            realmDat.setName(cols.get(2).select("b").text());
                            realmDat.setCabinet(cols.get(2).select("a").text() + cols.get(2).ownText());
                            realmDat.setTime(cols.get(1).text());
                            realmDat.setWeek(true);
                            realm.commitTransaction();
                        }

                        if (cols.size() > 3 && cols.get(3).hasText()) {
                            realm.beginTransaction();
                            realmData realmDat = realm.createObject(realmData.class);
                            realmDat.setDay(daycounter);
                            realmDat.setName(cols.get(3).select("b").text());
                            realmDat.setCabinet(cols.get(3).select("a").text() + cols.get(3).ownText());
                            realmDat.setTime(cols.get(1).text());
                            realmDat.setWeek(true);
                            realm.commitTransaction();
                        }

                        realm.beginTransaction();
                        realmData realmDat = realm.createObject(realmData.class);
                        realmDat.setDay(daycounter);
                        realmDat.setName(cols.get(2).select("b").text());
                        realmDat.setCabinet(cols.get(2).select("a").text() + cols.get(2).ownText());
                        realmDat.setTime(cols.get(1).text());
                        realmDat.setWeek(false);
                        realm.commitTransaction();


                    }




                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

        });
    }
}









