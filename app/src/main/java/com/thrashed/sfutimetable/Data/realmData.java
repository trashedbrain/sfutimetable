package com.thrashed.sfutimetable.Data;

import io.realm.RealmObject;

public class realmData extends RealmObject {

    private boolean week;
    private String name;
    private String cabinet;
    private String time;
    private int day;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCabinet() {
        return cabinet;
    }

    public void setCabinet(String cabinet) {
        this.cabinet = cabinet;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean getWeek() {
        return week;
    }

    public void setWeek(boolean nedelya) {
        this.week = nedelya;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
