package com.thrashed.sfutimetable.Adapters;


import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.thrashed.sfutimetable.Data.Lesson;
import com.thrashed.sfutimetable.R;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.LessonsViewHolder> {

    public static class LessonsViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView name;
        TextView cabinet;
        TextView time;

        LessonsViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            name = itemView.findViewById(R.id.Predmet);
            cabinet = itemView.findViewById(R.id.cabinet);
            time = itemView.findViewById(R.id.time);
        }
    }

    List<Lesson> lessons;

    public RVAdapter(List<Lesson> lesson) {
        this.lessons = lesson;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public LessonsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        LessonsViewHolder lvh = new LessonsViewHolder(v);
        return lvh;
    }

    @Override
    public void onBindViewHolder(LessonsViewHolder LessonsViewHolder, int i) {
        LessonsViewHolder.name.setText(lessons.get(i).name);
        LessonsViewHolder.cabinet.setText(lessons.get(i).cabinet);
        LessonsViewHolder.time.setText(lessons.get(i).time);
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }
}
