package com.thrashed.sfutimetable.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.thrashed.sfutimetable.Fragments.ChFragment;
import com.thrashed.sfutimetable.Fragments.PnFragment;
import com.thrashed.sfutimetable.Fragments.PtFragment;
import com.thrashed.sfutimetable.Fragments.SbFragment;
import com.thrashed.sfutimetable.Fragments.SrFragment;
import com.thrashed.sfutimetable.Fragments.VtFragment;


public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PnFragment();
            case 1:
                return new VtFragment();
            case 2:
                return new SrFragment();
            case 3:
                return new ChFragment();
            case 4:
                return new PtFragment();
            case 5:
                return new SbFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
